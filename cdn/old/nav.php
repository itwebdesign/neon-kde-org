<?
/**
 * Copyright 2016  Kenneth Vermette <vermette@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

	function getContent ($document) {
		return !file_exists($document) ?
			'No Content' :
			trim(file_get_contents($document));
	}

	$tempNavFiles = [
		'community' => getContent('htmlAssets/community.htm'),
		'products'  => getContent('htmlAssets/products.htm'),
		'support'   => getContent('htmlAssets/support.htm'),
		'develop'   => getContent('htmlAssets/develop.htm'),
		'donate'    => getContent('htmlAssets/donate.htm'),
	];

	$content = json_encode([
		'topLevelTabs' => $tempNavFiles
	]);

	if ($_GET['callback']) {
		if (preg_match('#^[a-z_$][a-z0-9_]+$#i', $_GET['callback'])) {
			die($_GET['callback'].'('.$content.');');
		}
		else {
			die('/* Invalid callback function */');
		}
	}
	else if ($_GET['embed']) {
		if (preg_match('#^[a-z_$][a-z0-9_]+$#i', $_GET['embed'])) {
			die($_GET['embed'].' = '.$content.';');
		}
		else {
			die('/* Invalid variable name */');
		}
	}
	else {
		die($content);
	}

?>