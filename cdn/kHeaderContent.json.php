<?php
/**
 * Copyright 2016  Kenneth Vermette <vermette@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * TODO: We should cache these files when we know the final environment.
 */
	require_once('core.php');


	$output = [];

	if (isset($_GET['include']))
		$include = explode(',', $_GET['include']);


	foreach ($include as &$i)
		$i = trim($i);


	if (in_array('menus', $include)) {
		$output['menus'] = [];
		$menus = ['community', 'products', 'support', 'develop', 'donate'];

		foreach ($menus as $m) {
			if (file_exists('htmlAssets/'.$m.'.htm'))
				$output['menus'][$m] = str_replace('local://', url(), file_get_contents('htmlAssets/'.$m.'.htm'));
		}
	}


	die(json_encode($output));
